from aevol.models.environment import Environment

import numpy as np
import pandas as pd

def test_init_from_json_file():
    simple_env_pts_df = pd.DataFrame({
        'x': np.arange(0, 1.1, .1),
        'y': [0.0, 0.1, 0.2, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]})

    json_file = 'json_test/environment_simple.json'
    envir = Environment.from_json_file(json_file)
    pd.testing.assert_frame_equal(envir.target.points, simple_env_pts_df)
