# ************************************************************************
#                          test individual  -  test
# ************************************************************************

# ---------------------------------------------------------------- INCLUDE
import sys
 
# setting path
sys.path.append('../')
 
# importing
from aevol.models.individual import Individual

# ---------------------------------------------------------------- GLOBAL VARIABLE

genome_length = 200

rnas = {
        "begin": 15,
        "end": 55,
        "strand": "LEADING",
        "is_coding": False,
        "expression_level": 0.19999999999999996
      }

proteins = {
        "begin": 352,
        "end": 375,
        "strand": "LEADING",
        "m": 0.2857142857142857,
        "w": 0.033333333,
        "h": 1.0,
        "expression_level": 0.19999999999999996
      }

phenotype = [{
          "x": 1,
          "y": 0.5
        },
        {
          "x": 0.1,
          "y": 0.2
        }]

# ---------------------------------------------------------------- TEST

# Test that rna are well initialized
def test_init_individual():
    indiv = Individual(genome_length, rnas, proteins, phenotype)
    assert indiv.genome_length == 200
    assert indiv.rnas== rnas
    assert indiv.proteins== proteins
    assert indiv.phenotype == phenotype