from aevol.models.protein import Protein

genome_length = 100
protein1 = Protein(25, 25, "LAGGING", 1, 0, 1, 0.5) #same beginning and ending position
protein2 = Protein(100, 0, "LAGGING", 2, 0.2, 1, 0.7) #equivalent to same beginning and ending position (100=genome_lentgh)
protein3 = Protein(1, 100, "LAGGING", 0.5, 1, 0.3, 1) #from 1 to genome_length

# Test that proteins are well initialized
def test_init_protein():
    assert protein1.begin == 25
    assert protein1.end == 25
    assert protein1.strand=="LAGGING"
    assert protein1.m == 1
    assert protein1.h == 1
    assert protein1.w == 0
    assert protein1.expression_level == 0.5

# Test that the angles of the protein representation are well computed
def test_compute_angles():
    # same beginning and ending position
    theta1, theta2 = protein1.compute_angles(genome_length)
    assert theta1 == 90
    assert theta2 == 90 - 0.0001

    # from 0 to genome_length
    theta1, theta2 = protein2.compute_angles(genome_length)
    assert theta1 == 360
    assert theta2 == 0
