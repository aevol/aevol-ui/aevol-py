from aevol.models.rna import Rna

genome_length = 100
rna1 = Rna(25, 50, "LEADING", False) #leading, not coding
rna2 = Rna(75, 50, "LEADING", True) #leading, coding

# Test that rnas are well initialized
def test_init_rna():
    assert rna1.begin == 25
    assert rna1.end == 50
    assert rna1.is_coding == False
    assert rna1.strand == "LEADING"

# Test that the angles of the strands are well computed
def test_compute_angles():
    theta1, theta2, theta3 = rna1.compute_angles(genome_length)
    assert theta1 == 90
    assert theta2 == 180

    theta1, theta2, theta3 = rna2.compute_angles(genome_length)
    assert theta1 == 270
    assert theta2 == 180
