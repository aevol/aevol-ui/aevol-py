# ************************************************************************
#                          test population  -  test
# ************************************************************************

# ---------------------------------------------------------------- INCLUDE
import sys
 
# setting path
sys.path.append('../')
 
# importing
from aevol.models.population import Population


# ---------------------------------------------------------------- TEST

# Test that populations are well initialized
def test_init_population():
    grid_width = 2
    grid_height = 2

    fitness = [0.4, 0.2, 0.8, 0.1]

    population = Population(grid_width, grid_height, fitness)

    assert population.grid_height == 2
    assert population.grid_width== 2
    assert population.fitness_array == [0.4, 0.2, 0.8, 0.1]

# Test that fitness_grid is well reshaped into a 2D array
def test_compute_fitness_grid():
    
  fitness = [0.4, 0.2, 0.8, 0.1]
  population = Population(2, 2, fitness)
  
  response = population.compute_fitness_grid()
  assert response[0][0] == 0.4
  assert response[0][1] == 0.2
  assert response[1][0] == 0.8
  assert response[1][1] == 0.1

