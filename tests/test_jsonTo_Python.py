# ************************************************************************
#                          test json to python  -  test
# ************************************************************************

# ---------------------------------------------------------------- INCLUDE
from json import JSONDecodeError

import pandas as pd
import sys
 
# setting path
sys.path.append('../')
 
# importing
from aevol.models.individual import Individual
from aevol.models.population import Population


# ---------------------------------------------------------------- TEST

# Test that an error is raised when we input a file with a wrong path
def test_file_not_found():
    json = 'does_not_exist.json'
    try:
        Individual.from_json_file(json)
        assert False
    except FileNotFoundError:
        assert True

# Test that an error is raised when we input a file which is not a json
def test_bad_format():
    odt = 'json_test/other_format/doc.odt'
    pdf = 'json_test/other_format/pdf.pdf'
    jpg = 'json_test/other_format/img.jpg'
    
    response = True

    try:
        Individual.from_json_file(odt)
        response = False
    except UnicodeDecodeError:
        response = True
         
    try:
        Individual.from_json_file(pdf)
        response = False
    except UnicodeDecodeError:
        response = response

    try:
        Individual.from_json_file(jpg)
        response = False
    except UnicodeDecodeError:
        response = response

    assert response == True

# Test that an error is raised when we input an empty json file
def test_empty_json():
    json = 'json_test/test_empty_json.json'
    try:
        Individual.from_json_file(json)
        assert False
    except JSONDecodeError:
        assert True

# Test that an error is raised when we input json file with missing values
def test_json_missing_values():
    json_missing_values = 'json_test/test_missing_values.json'
    try:
        Individual.from_json_file(json_missing_values)
        assert False
    except Exception:
        assert True

# Test that an error is raised when genome length is lower or equal to 0
def test_genome_length_0():
    json_genome_length_0 = 'json_test/test_genome_length_0.json'
    try:
        Individual.from_json_file(json_genome_length_0)
        assert False
    except NameError as msg:
        assert True
        assert msg.args[0] == "The genome_length cannot be null or negative"

# Test that individual are well deserialized
def test_deserialize_individual():
    json = 'json_test/test_short_individual.json'
    individual = Individual.from_json_file(json)

    assert individual.genome_length == 3018

    assert individual.rnas[0].begin == 15
    assert individual.rnas[0].end == 55
    assert individual.rnas[0].strand == "LEADING"
    assert individual.rnas[0].is_coding == False
    
    assert individual.proteins[1].begin == 352
    assert individual.proteins[1].end == 375
    assert individual.proteins[1].strand == "LEADING"
    assert individual.proteins[1].m == 0.2857142857142857
    assert individual.proteins[1].w == 0.033333333
    assert individual.proteins[1].h == 1.0
    assert individual.proteins[1].expression_level == 0.19999999999999996

    phenotype_pts_df = pd.DataFrame({'x': [1.0, 0.1], 'y': [0.5, 0.2]})
    pd.testing.assert_frame_equal(individual.phenotype.points, phenotype_pts_df)

# Test that populations grid are well deserialized
def test_deserialize_population():
    json = 'json_test/test_short_population.json'
    population = Population.from_json_file(json)

    assert population.grid_height == 2
    assert population.grid_width == 2
    assert population.fitness_array == [0.4, 0.2, 0.8, 0.1]
