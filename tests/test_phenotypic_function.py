from aevol.models.phenotypic_function import PhenotypicFunction

import pandas as pd

default_pts_df = pd.DataFrame({'x': [0.0, 1.0], 'y': [0.0, 0.0]})
simple_pts = [[0., 0.], [0.5, 1], [1, 0]]
simple_pts_df = pd.DataFrame({'x': [0.0, 0.5, 1.0], 'y': [0.0, 1.0, 0.0]})

def test_init():
    default = PhenotypicFunction()
    pd.testing.assert_frame_equal(default.points, default_pts_df)

    simple = PhenotypicFunction(simple_pts)
    pd.testing.assert_frame_equal(simple.points, simple_pts_df)
