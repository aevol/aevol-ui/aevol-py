from aevol.models.rna import Rna
from aevol.models.protein import Protein
from aevol.visual.genome_view import GenomeView

# ---------------------------------------------------------------- DATA
genome_length = 100

rna1 = Rna(25, 50, "LEADING", False) #leading, not coding
rna2 = Rna(75, 50, "LEADING", True) #leading, coding

protein1 = Protein(25, 25, "LAGGING", 1, 0, 1, 0.5) #same beginning and ending position
protein2 = Protein(100, 0, "LAGGING", 2, 0.2, 1, 0.7) #equivalent to same beginning and ending position (100=genome_lentgh)
protein3 = Protein(1, 100, "LAGGING", 0.5, 1, 0.3, 1) #from 1 to genome_length

# ---------------------------------------------------------------- TESTS
# Test that the layers of the strands are well computed
def test_compute_layer():
    genome_view = GenomeView(3)
    theta1, theta2, theta3 = rna1.compute_angles(genome_length)
    assert genome_view.compute_layer(rna1.strand, theta1, theta2) == 1
    theta1, theta2, theta3 = rna2.compute_angles(genome_length)
    assert genome_view.compute_layer(rna2.strand, theta1, theta2) == 2

    genome_view = GenomeView(3)
    theta1, theta2 = protein1.compute_angles(genome_length)
    assert genome_view.compute_layer(protein1.strand, theta1, theta2) == -1
    theta1, theta2 = protein2.compute_angles(genome_length)
    assert genome_view.compute_layer(protein2.strand, theta1, theta2) == -1
    theta1, theta2 = protein3.compute_angles(genome_length)
    assert genome_view.compute_layer(protein3.strand, theta1, theta2) == -2
