How to make a new release of aevol-py
=====================================

Bump the package's version
--------------------------

Use poetry to bump the version (chose between patch, minor and major), then commit the change

e.g. if poetry told you it was bumping to version 0.4.0:

```sh
$ poetry version minor
$ git commit pyproject.toml -m 0.4.0
```

Tag the commit corresponding to the version to be released
----------------------------------------------------------

e.g. for version 0.4.0:

```sh
$ git tag -a v0.4.0 -m 0.4.0
```

Build the new version of the package
------------------------------------

```sh
poetry build
```

Publish the new version of the package
------------------------------------

```sh
poetry publish
```
