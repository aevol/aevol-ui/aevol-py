Python Package for Aevol
========================

This is a Python Package for Aevol (https://aevol.fr/).

It contains data models, code for creating visualizations and command-line scripts to be used with aevol-generated data.


Contributing
------------

Probably the easiest way of trying out any modification you would like to make to this package is to install an **editable** version of the package.

This can be done by running the following command from the topmost directory of the project (where this README sits):

```sh
$ pip install --editable .
```
